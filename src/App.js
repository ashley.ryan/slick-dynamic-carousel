import {useEffect, useState, useRef} from 'react'
import { Carousel, CarouselSlide, PromoBar, Typography } from '@vp/swan';
import ToolTip from 'react-portal-tooltip';
import logo from './logo.svg';
import './App.css';
import { SwanConfiguration } from './SwanConfiguration';

function Wrapper({children}) {
  const ref = useRef();
  const [renderTooltip, setRenderTooltip] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setRenderTooltip(true);
    }, 2000)
  });

  console.log({ref})

  if(renderTooltip) {
    return (
      <>
        <div ref={ref} style={{height: 20}}/>
        {children}
        <ToolTip active={true} position="top" arrow="center" parent={ref.current}>
        I'm a dynamic tooltip!
        </ToolTip>
      </>
    );
  }

  return children
}

function App() {
  const texts = ['Text 1', 'Text 2'];


  return (
    <SwanConfiguration>
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <main>
        <Carousel
          skin="promo-bar"
          fade
          accessibleTextForPrevious="Go to previous slide"
          accessibleTextForNext="Go to next slide"
        >
          {texts.map(text => (
              <CarouselSlide key={text}>
                <Wrapper>
                
                  <Typography as="p" my={0}>
                    {text}
                  </Typography>
                  <a href="#">Learn More</a>

                </Wrapper>
              </CarouselSlide>
            ))
          }
        </Carousel>
      </main>
    </div>
    </SwanConfiguration>
  );
}

export default App;

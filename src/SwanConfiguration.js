import * as React from 'react'
import { Helmet } from 'react-helmet'
import {
  SwanProvider,
  SwanHead,
  SWAN_STYLE_KEY_MAP,
  SWAN_BASE_URL_MAP,
  useBrowserClasses,
  getRootClassNames,
} from '@vp/swan'

const styleKeys = [
  SWAN_STYLE_KEY_MAP.carousel
  // List all the required keys, Doesn't autoload keys now
]

export const SwanConfiguration = ({ children }) => {
  const browserClassName = useBrowserClasses()
  const bodyClassName = getRootClassNames()
  return (
    <SwanProvider
      swanTenant="vistaprint"
      swanLocale="en-US"
    >
      <Helmet
        htmlAttributes={{ class: browserClassName }}
        bodyAttributes={{ class: bodyClassName }}
      />
      <SwanHead
        renderWith={Helmet}
        renderStyleContentAsChildren
        styleSheetKeys={styleKeys}
      />
      {children}
    </SwanProvider>
  )
}
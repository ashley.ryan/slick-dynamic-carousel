# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Install

`npm install --force` to bypass react + swan version mismatch

## Serve Production build

`npm run build`
`npm install -g serve`
`server -s build`